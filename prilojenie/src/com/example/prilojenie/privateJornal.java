package com.example.prilojenie;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class PrivateJornal extends Activity {

	ListView lvData;
	DB db;
	SimpleCursorAdapter scAdapter;
	Cursor cursor;
	String line = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.privatejornal);

		// Открытие бд
		db = new DB(this);
		db.open();

		// получить все данные из таблицы
		cursor = db.getAllData();
		startManagingCursor(cursor);

		// Создание масивов from и  to для адаптера
		String[] from = new String[] { DB.COLUMN_DATE, DB.COLUMN_KM,
				DB.COLUMN_NAME, DB.COLUMN_COL, DB.COLUMN_CENA, DB.COLUMN_SUMM };
		int[] to = new int[] { R.id.ViewData, R.id.ViewProbeg, R.id.ViewOpis,
				R.id.ViewKol, R.id.ViewCena, R.id.ViewSumm };

		// вызов адаптера
		scAdapter = new SimpleCursorAdapter(this, R.layout.item, cursor, from,
				to);
		lvData = (ListView) findViewById(R.id.lvData);
		lvData.setAdapter(scAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add, menu);
		return true;

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		Intent intent_registration = new Intent(this, Addnewrecord.class);
		startActivity(intent_registration);
		return super.onOptionsItemSelected(item);
	}

	protected void onDestroy() {
		super.onDestroy();
		// Закрытие БД
		db.close();
	}

}
