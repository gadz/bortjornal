package com.example.prilojenie;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnClickListener {

	ImageView image1 ;
	ImageView image2 ;
	ImageView image3 ;
	ImageView image4 ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		image1 = (ImageView) findViewById(R.id.imageView2);
		image1.setOnClickListener(this);
		
		image2 = (ImageView) findViewById(R.id.ImageView02);
		image2.setOnClickListener(this);
		
		image3 = (ImageView) findViewById(R.id.ImageView01);
		image3.setOnClickListener(this);
		
		image4 = (ImageView) findViewById(R.id.ImageView04);
		image4.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item) {
	      // TODO Auto-generated method stub
	  	Intent intent_registration = new Intent(this, Registration.class);
        startActivity(intent_registration);
	      return super.onOptionsItemSelected(item);
	    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		 switch (v.getId()) {
		    case R.id.imageView2:
		      // TODO Call second activity
		    	Intent intent_privateJornal = new Intent(this, PrivateJornal.class);
		        startActivity(intent_privateJornal);
		      break;
			    
		      case R.id.ImageView02:
			      // TODO Call second activity
			    	Intent intent_katalog = new Intent(this, Katalog.class);
			        startActivity(intent_katalog);
			      break;
				  
			  case R.id.ImageView01:
				      // TODO Call second activity
				    	Intent intent_memory = new Intent(this, Memory.class);
				        startActivity(intent_memory);
				      break;
				
		    case R.id.ImageView04:
					      // TODO Call second activity
					    	Intent intent_settings = new Intent(this, Settings.class);
					        startActivity(intent_settings);
					      break;
		      
		    default:
		      break;
		    }
	}

}
