package com.example.prilojenie;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.drag.DragAndDropItem;
import ru.yandex.yandexmapkit.overlay.drag.DragAndDropOverlay;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Addnewrecord extends Activity implements OnClickListener {

	public static final String LOG_TAG = "myLogs";
	Button addAdd;
	Button addClear;
	// Button addRead;
	EditText addData;
	EditText addProbeg;
	EditText addOpis;
	EditText addKol;
	EditText addCena;
	EditText addSumm;
	// EditText addNote;

	DBHelper dbHelper;

	//
	private MapView mMapView;
	private OverlayItem mTapItem;
	private customOverlay mOverlay;
	private MapController mMapController;
	private OverlayManager mOverlayManager;
	private Coordinate mX;
	private Coordinate mY;
	// private String mDecimalSeparator;
	private Drawable mTapItemBitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addnewrecord);

		addAdd = (Button) findViewById(R.id.addAdd);
		addAdd.setOnClickListener(this);

		addClear = (Button) findViewById(R.id.addClear);
		addClear.setOnClickListener(this);

		// addRead = (Button) findViewById(R.id.butRead);
		// addRead.setOnClickListener(this);

		addData = (EditText) findViewById(R.id.editTextLogin);
		addProbeg = (EditText) findViewById(R.id.editTextAuto);
		addOpis = (EditText) findViewById(R.id.editText3);
		addKol = (EditText) findViewById(R.id.editText4);
		addCena = (EditText) findViewById(R.id.editText5);
		addSumm = (EditText) findViewById(R.id.editText6);

		// ������� ������ ��� �������� � ���������� �������� ��
		dbHelper = new DBHelper(this);

		
		mMapView = (MapView) findViewById(R.id.map);
		// mDecimalSeparator = ".";
		if (savedInstanceState == null) {
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
			// по умолчанию ставятся координаты пригорода Парижа
			mX = new Coordinate(pref.getString("x", "47.4713"));
			mY = new Coordinate(pref.getString("y", "42.9827"));
		} else {
			// восстановление координат
			mX = savedInstanceState.getParcelable("geox");
			mY = savedInstanceState.getParcelable("geoy");
		}

		mMapController = mMapView.getMapController();
		mOverlayManager = mMapController.getOverlayManager();
		mMapController.setZoomCurrent(15);
		// создание своего слоя
		mOverlay = new customOverlay(mMapController);
		mOverlayManager.addOverlay(mOverlay);
		// установка тайла(метки)
		//здесь ошибка возникает при повороте экрана в  setManualItem();
		setManualItem();

	}

	class DBHelper extends SQLiteOpenHelper {

		public DBHelper(Context context) {
			// ����������� �����������
			super(context, "myDB", null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.d(LOG_TAG, "--- onCreate database ---");
			// создание бд
			db.execSQL("create table bort ("
					+ "_id integer primary key autoincrement," + "date text,"
					+ "km integer," + "name text," + "kol integer,"
					+ "cena integer," + "summ integer," + "prim text,"
					+ "user text" + ");");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		// ������� ������ ��� ������
		ContentValues cv = new ContentValues();

		// �������� ������ �� ����� �����
		String Data = addData.getText().toString();
		String Probeg = addProbeg.getText().toString();
		String Opis = addOpis.getText().toString();
		String Kol = addKol.getText().toString();
		String Cena = addCena.getText().toString();
		String Summ = addSumm.getText().toString();
		// String Note = addNote.getText().toString();

		// ������������ � ��
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		switch (v.getId()) {
		case R.id.addAdd:
			Log.d(LOG_TAG, "--- Insert in mytable: ---");
			// ���������� ������ ��� ������� � ���� ���: ������������ ������� -
			// ��������

			cv.put("date", Data);
			cv.put("km", Probeg);
			cv.put("name", Opis);
			cv.put("kol", Kol);
			cv.put("cena", Cena);
			cv.put("summ", Summ);
			// cv.put("prim", Note);

			// ��������� ������ � �������� �� ID
			long rowID = db.insert("bort", null, cv);
			Log.d(LOG_TAG, "row inserted, ID = " + rowID);
			break;

		// Эту функцию (чтения записей из бд) отменяем , нужна была при отладке
		/*
		 * case R.id.butRead: Log.d(LOG_TAG, "--- Rows in mytable: ---"); //
		 * ������ ������ ���� ������ �� ������� mytable, �������� Cursor Cursor
		 * c = db.query("bort", null, null, null, null, null, null);
		 * 
		 * // ������ ������� ������� �� ������ ������ ������� // ���� � �������
		 * ��� �����, �������� false if (c.moveToFirst()) {
		 * 
		 * // ���������� ������ �������� �� ����� � ������� int idColIndex =
		 * c.getColumnIndex("_id"); int dateColIndex = c.getColumnIndex("date");
		 * int kmColIndex = c.getColumnIndex("km"); int nameColIndex =
		 * c.getColumnIndex("name"); int kolColIndex = c.getColumnIndex("kol");
		 * int cenaColIndex = c.getColumnIndex("cena"); int summColIndex =
		 * c.getColumnIndex("summ"); int primColIndex =
		 * c.getColumnIndex("prim");
		 * 
		 * do { // �������� �������� �� ������� �������� � ����� ��� � ���
		 * Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex) + ", date = " +
		 * c.getString(dateColIndex) + ", km = " + c.getString(kmColIndex) +
		 * ", name = " + c.getString(nameColIndex) + ", kol = " +
		 * c.getString(kolColIndex) + ", cena = " + c.getString(cenaColIndex) +
		 * ", summ = " + c.getString(summColIndex) + ", prim = " +
		 * c.getString(primColIndex)
		 * 
		 * ); // ������� �� ��������� ������ // � ���� ��������� ��� (������� -
		 * ���������), �� false - // ������� �� ����� } while (c.moveToNext());
		 * } else Log.d(LOG_TAG, "0 rows"); c.close(); break;
		 */

		case R.id.addClear:

			break;
		}
		// Закрытие бд
		dbHelper.close();

	}

	/*
	 * Самодельный слой, наследуется от слоя по которому можно тыкать и тащить
	 */
	private class customOverlay extends DragAndDropOverlay {

		public customOverlay(MapController arg0) {
			super(arg0);
		}

		@Override
		public boolean onSingleTapUp(float arg0, float arg1) {
			if (mTapItem != null) {
				// удаление старой метки(тайла)
				mOverlay.removeOverlayItem(mTapItem);
			}
			// получение координат экрана в рамках вьювера карты
			ScreenPoint s = new ScreenPoint(arg0, arg1);
			// преобразование экранных координат в географические
			GeoPoint g = mMapController.getGeoPoint(s);
			// создание и добавление новой метки(тайла)
			mTapItem = new DragAndDropItem(g, mTapItemBitmap);
			mOverlay.addOverlayItem(mTapItem);
			mMapController.setPositionNoAnimationTo(g);
			// переозначивание координат
			mX = new Coordinate(mTapItem.getGeoPoint().getLon());
			mY = new Coordinate(mTapItem.getGeoPoint().getLat());
			// взведение флага блокировки для предотвращения
			// рекурсивного вызова ручного обновления координат
			// mFlgBlockedTextChange = true;
			/*
			 * if (mX.isValid()) mEditX.setText(mX.toString().replace(".",
			 * mDecimalSeparator)); if (mY.isValid())
			 * mEditY.setText(mY.toString().replace(".", mDecimalSeparator));
			 */
			// mFlgBlockedTextChange = false;
			return super.onSingleTapUp(arg0, arg1);
		};

	}

	/*
	 * метод ручной установки метки(тайла) на карте
	 */
	private void setManualItem() {
		// установка координат в ручную
		//проверить это условие, тут ошибка выходит
		if (mX.isValid() && mY.isValid()) {
			if (mTapItem != null) {
				// удаление старого тайла(метки), если она есть
				mOverlay.removeOverlayItem(mTapItem);
			}
			
			GeoPoint g = new GeoPoint(mY.GetValue(), mX.GetValue());
		//	GeoPoint g = new GeoPoint(47.5, 43.5);

			// перемещаем карту на заданные координаты
			mMapController.setPositionNoAnimationTo(g);
			mMapController.setZoomCurrent(15);
			Resources res = getResources();
			mTapItemBitmap = res.getDrawable(R.drawable.drag1);
			// создание нового тайла(метки) на карте
			mTapItem = new DragAndDropItem(g, mTapItemBitmap);
			mOverlay.addOverlayItem(mTapItem);
			mOverlayManager.getMyLocation().setEnabled(false); 
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		// Закрытие бд
		dbHelper.close();
	}

}