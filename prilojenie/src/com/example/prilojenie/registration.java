package com.example.prilojenie;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Registration extends Activity implements OnClickListener {

	protected static final String Tag = null;
	private Button btnRegistration;
	protected String line;
	private Handler h;
	private EditText editTextLogin;
	private EditText editTextAuto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration);

		// найдем View-элементы

		btnRegistration = (Button) findViewById(R.id.buttonRegistration);

		editTextLogin = (EditText) findViewById(R.id.editTextLogin);
		editTextAuto = (EditText) findViewById(R.id.editTextAuto);
		// присваиваем обработчик кнопкам
		btnRegistration.setOnClickListener(this);

		h = new Handler() {
			public void handleMessage(android.os.Message msg) {
				// тут делаем кнопку нажатой после получения данных с сервера
				// btnRefresh.setText("Получено" + msg.what);
				btnRegistration.setEnabled(false);
			

				// Log.d(Tag, line);
			};
		};

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		final String login = editTextLogin.getText().toString();
		final String auto = editTextAuto.getText().toString();

		// TODO Auto-generated method stub
		// по id определеяем кнопку, вызвавшую этот обработчик
		switch (v.getId()) {
		case R.id.buttonRegistration:
			// кнопка ОК
			// tvOut.setText("Нажата кнопка ОК");

			Thread t = new Thread(new Runnable() {
				public void run() {

					DefaultHttpClient client = new DefaultHttpClient(); // создаем
																		// клиента
					try {
						HttpResponse response = client.execute(new HttpGet(
								"http://vitoelexir.ru/android/adduser.php?var1="
										+ login + "&var2=" + auto)); // пытаемся
						// выполнить
						// GET
						// запрос
						HttpEntity httpEntity = response.getEntity(); // получаем
																		// ответ
																		// (содержимое
																		// страницы
																		// вывода)
						line = EntityUtils.toString(httpEntity, "UTF-8");
						Log.d(Tag, line);

					} catch (Exception e) {
						Log.i("Request exception",
								"Exception: " + e.getMessage()); // Oops
					}

					h.sendEmptyMessage(5);
				}
			});
			t.start();

			// btnRefresh.setText("test");

			break;

		}

	}
}
