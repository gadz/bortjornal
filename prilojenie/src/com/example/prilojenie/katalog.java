package com.example.prilojenie;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class Katalog extends Activity implements OnClickListener {

	protected static final String Tag = null;
	Button btnRefresh;
	private Handler h;
	String line;

	private static final String FIRST = "logins";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.katalog);

		// найдем View-элементы
		btnRefresh = (Button) findViewById(R.id.buttonRefresh);

		
		final ListView listView = (ListView) findViewById(R.id.listView1);

		// присваиваем обработчик кнопкам
		btnRefresh.setOnClickListener(this);

		final ArrayList<HashMap<String, Object>> myBooks = new ArrayList<HashMap<String, Object>>();

		h = new Handler() {
			public void handleMessage(android.os.Message msg) {
				// тут делаем кнопку нажатой после получения данных с сервера
				// btnRefresh.setText("Получено" + msg.what);
				btnRefresh.setEnabled(false);
				// if (msg.what == 10) btnStart.setEnabled(true);

				// тут мы обрабатываем json данные и вытаскиваем из них логины
				// зарегистрированных людей

				try {
					// создали читателя json объектов и отдали ему строку -
					// result
					JSONObject json = new JSONObject(line);
					// дальше находим вход в наш json им является ключевое слово
					// data
					JSONArray urls = json.getJSONArray("data");
					// проходим циклом по всем нашим параметрам
					for (int i = 0; i < urls.length(); i++) {
						HashMap<String, Object> hm;
						hm = new HashMap<String, Object>();
						// читаем что в себе хранит параметр firstname
						hm.put(FIRST, urls.getJSONObject(i).getString("login")
								.toString());
						myBooks.add(hm);
					}
					// дальше добавляем полученные параметры в наш адаптер
					SimpleAdapter adapter = new SimpleAdapter(Katalog.this,
							myBooks, R.layout.list, new String[] { FIRST },
							new int[] { R.id.text1 });
					// выводим в листвью
					listView.setAdapter(adapter);
					// listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

				} catch (JSONException e) {
					Log.e("log_tag", "Error parsing data " + e.toString());
				}

				// Log.d(Tag, line);
			};
		};

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// по id определеяем кнопку, вызвавшую этот обработчик
		switch (v.getId()) {
		case R.id.buttonRefresh:
			// кнопка ОК
			// tvOut.setText("Нажата кнопка ОК");

			Thread t = new Thread(new Runnable() {
				public void run() {

					DefaultHttpClient client = new DefaultHttpClient(); // создаем
																		// клиента
					try {
						HttpResponse response = client.execute(new HttpGet(
								"http://vitoelexir.ru/android/bdtest.php")); // пытаемся
																		// выполнить
																		// GET
																		// запрос
						HttpEntity httpEntity = response.getEntity(); // получаем
																		// ответ
																		// (содержимое
																		// страницы
																		// вывода)
						line = EntityUtils.toString(httpEntity, "UTF-8");
						// Log.d(Tag, line);

					} catch (Exception e) {
						Log.i("Request exception",
								"Exception: " + e.getMessage()); // Oops
					}

					h.sendEmptyMessage(5);
				}
			});
			t.start();

			// btnRefresh.setText("test");

			break;
		
		}
	}
}
